using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProtagonistController : MonoBehaviour
{
    public bool shoot;
    public bool isRunning;
    public bool die;
    private static readonly int Shoot = Animator.StringToHash("shoot");
    private static readonly int IsRunning = Animator.StringToHash("isRunning");
    private static readonly int Die = Animator.StringToHash("die");

    void Start()
    {
        shoot = false;
        isRunning = false;
        die = false;
    }
    
    void Update()
    {
        if (shoot)
        {
            GetComponent<Animator>().SetTrigger(Shoot);
            shoot = false;
        }
        GetComponent<Animator>().SetBool(IsRunning, isRunning);
        if (die)
            GetComponent<Animator>().SetTrigger(Die);
    }
}
